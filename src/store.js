import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './reducers/';
import rootSaga from './sagas';
import { products } from './mocks/';

const defaultState = {
  products,
  app: {
    locale: null // retrieve from client (navigator.language) if not set
  }
};

const sagaMiddleware = createSagaMiddleware();
const store = createStore(rootReducer, defaultState, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

if(module.hot) {
  module.hot.accept('./reducers/',() => {
    const nextRootReducer = require('./reducers/').default;
    store.replaceReducer(nextRootReducer);
  });
}

export default store;