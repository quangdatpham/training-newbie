import React from 'react';
import PropTypes from 'prop-types';

export default function ProductItem({ _id, name, price, handleOnDelete }) {
  return (
    <div className="ProductItem">
      <p>ID: { _id }</p>
      <p>NAME: { name }</p>
      <p>PRICE: { price }</p>
      <p><button onClick={handleOnDelete}>Delete</button></p>
    </div>
  );
}

ProductItem.propTypes = {
  _id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]).isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  handleOnDelete: PropTypes.func.isRequired
}